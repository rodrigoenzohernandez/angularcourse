import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-binding',
  templateUrl: './binding.component.html',
  styleUrls: ['./binding.component.scss']
})
export class BindingComponent implements OnInit {

  userName=""

  constructor() { }

  ngOnInit(): void {
  }

  resetUsername(){
    this.userName = "";
  }

}
