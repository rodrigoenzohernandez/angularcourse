import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directives',
  templateUrl: './directives.component.html',
  styleUrls: ['./directives.component.scss']
})
export class DirectivesComponent implements OnInit {

  hidden = true;
  numbersList = [];

  constructor() { }

  ngOnInit(): void {
  }

  onClick(){
    this.hidden = !this.hidden;
    const lastNumber = this.numbersList.length;
    this.numbersList.push(lastNumber +1)
  }

}
